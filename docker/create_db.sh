#!/bin/bash
docker image build . -t postgres-msa-db-init:latest
docker run --name postgres-msa-db \
  -e POSTGRES_DB=postgres-msa-db \
  -e POSTGRES_USER=postgres-user \
  -e POSTGRES_PASSWORD=postgres-user-pass \
  -v /tmp/msa/db/postgres/postgres-data:/var/lib/postgresql/data \
  -p 5001:5432 \
  postgres-msa-db-init:latest

read -r -p "Нажмите ENTER для продолжения"
