package ru.yumashev.skillbox.msa.users.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping("/subscriptions")
public interface SubscriptionsController {

    @PostMapping("/{subscriberId}/to/{subscriptionId}")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Подписка")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    void createSubscription(@PathVariable UUID subscriberId, @PathVariable UUID subscriptionId);

    @DeleteMapping("/{subscriberId}/to/{subscriptionId}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Отписка")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    void deleteSubscription(@PathVariable UUID subscriberId, @PathVariable UUID subscriptionId);

}
