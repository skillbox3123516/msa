package ru.yumashev.skillbox.msa.users.controllers.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import ru.yumashev.skillbox.msa.users.controllers.SubscriptionsController;
import ru.yumashev.skillbox.msa.users.usecases.SubscriptionsUseCase;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class SubscriptionsControllerImpl implements SubscriptionsController {

    private final SubscriptionsUseCase subscriptionsUseCase;

    @Override
    public void createSubscription(UUID subscriberId, UUID subscriptionId) {
        subscriptionsUseCase.subscribe(subscriberId, subscriptionId);
    }

    @Override
    public void deleteSubscription(UUID subscriberId, UUID subscriptionId) {
        subscriptionsUseCase.unsubscribe(subscriberId, subscriptionId);
    }
}
