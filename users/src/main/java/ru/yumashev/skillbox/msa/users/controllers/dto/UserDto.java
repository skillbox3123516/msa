package ru.yumashev.skillbox.msa.users.controllers.dto;

import lombok.Builder;
import lombok.Data;
import ru.yumashev.skillbox.msa.users.entities.Gender;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
public class UserDto {
    private UUID id;
    private String firstName;
    private String lastName;
    private String middleName;
    private Gender gender;
    private LocalDate birthDate;
    private String iconUrl;
    private String about;
    private String nickname;
    private String email;
    private String phone;
    private CityDto city;
}
