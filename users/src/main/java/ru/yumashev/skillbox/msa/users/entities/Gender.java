package ru.yumashev.skillbox.msa.users.entities;

public enum Gender {
    MALE,
    FEMALE
}
