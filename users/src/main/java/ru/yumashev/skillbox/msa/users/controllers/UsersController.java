package ru.yumashev.skillbox.msa.users.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.yumashev.skillbox.msa.users.controllers.dto.CreateUserDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UpdateUserDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UserDto;

import java.util.UUID;

@RequestMapping("/users")
public interface UsersController {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Создание пользователя")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = CreateUserDto.class))),
                    @ApiResponse(responseCode = "400", description = "Неправильный формат входных параметров", content = @Content()),
                    @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    UUID createUser(CreateUserDto createUserDto);

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Обновление пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Пользователь не найден", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    void updateUser(@PathVariable UUID id, UpdateUserDto updateUserDto);

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Удаление пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Пользователь не найден", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    void deleteUser(@PathVariable UUID id);

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Получение пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Пользователь не найден", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    UserDto getUser(@PathVariable UUID id);

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Получение пользователей")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    Page<UserDto> getAllUsers(@RequestParam(required = false) Pageable pageable);
}
