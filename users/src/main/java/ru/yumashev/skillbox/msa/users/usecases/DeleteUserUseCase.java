package ru.yumashev.skillbox.msa.users.usecases;

import java.util.UUID;

public interface DeleteUserUseCase {
    void delete(UUID id);
}
