package ru.yumashev.skillbox.msa.users.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "country")
@Getter
@Setter
public class Country extends UuidSuperEntity {

    @Column(name = "name", nullable = false, length = 256)
    private String name;

    @Column(name = "iso_code_3", nullable = false, length = 3)
    private String isoCode3;
}
