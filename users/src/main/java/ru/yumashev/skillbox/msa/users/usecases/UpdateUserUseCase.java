package ru.yumashev.skillbox.msa.users.usecases;

import ru.yumashev.skillbox.msa.users.controllers.dto.UpdateUserDto;

import java.util.UUID;

public interface UpdateUserUseCase {
    void update(UUID id, UpdateUserDto updateUserDto);
}
