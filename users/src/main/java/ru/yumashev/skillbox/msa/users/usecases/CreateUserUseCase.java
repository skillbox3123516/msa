package ru.yumashev.skillbox.msa.users.usecases;

import ru.yumashev.skillbox.msa.users.controllers.dto.CreateUserDto;
import ru.yumashev.skillbox.msa.users.entities.User;

public interface CreateUserUseCase {
    User create(CreateUserDto createUserDto);
}
