package ru.yumashev.skillbox.msa.users.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import ru.yumashev.skillbox.msa.users.entities.Subscription;
import ru.yumashev.skillbox.msa.users.entities.User;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, UUID> {
    Optional<Subscription> findBySubscriberAndSubscription(@NonNull User subscriber, @NonNull User subscription);

}